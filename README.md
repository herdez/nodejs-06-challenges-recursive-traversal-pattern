# NodeJS Challenges (Instructions) - Solving Problems  

## NodeJS's event-driven APIs and Recursive Traversal

### Objectives

- To use NodeJS's event-driven APIs.
- To use the CommonJS Module Pattern.
- To use `Asynchronous Recursion with Callbacks`.
- To build NodeJS scripts with usage of `recursive traversal` and the helper methods `.filter()` and `.map()`.

### Description

Write two modules `readData.js` and `where.js` with the following responsabilities respectively:

- A node script that will read JSON files from `data` folder and create an array with objects.

The following is the JSON files structure:

```
//data1.json
{
    "data": [
                { "title": "Cymbeline", 
                  "author": "Shakespeare", 
                  "year": 1623 
                },
                { "title": "The Tempest", 
                  "author": "Shakespeare", 
                  "year": 1623 
                },
                { "title": "Hamlet", 
                  "author": "Shakespeare", 
                  "year": 1603 
                }
            ]
}

//data2.json
{
    "data": [
                { "title": "The Maid in the Mill", 
                  "author": "Fletcher", 
                  "year": 1623 
                },
                { "title": "A Streetcar Named Desire", 
                  "author": "Williams", 
                  "year": 1947 
                },
                { "title": "King Lear", 
                  "author": "Shakespeare", 
                  "year": 1608 
                }
            ]
}

//data3.json
{
    "data": [
                { "title": "A Midsummer Night's Dream",
                  "author": "Shakespeare", 
                  "year": 1600 
                },
                { "title": "Macbeth", 
                  "author": "Shakespeare", 
                  "year": 1623 
                },
                { "title": "Glass Menagerie", 
                  "author": "Williams", 
                  "year": 1944 
                }
            ]
}

//data4.json
{
    "data": [
                { "title": "The Misunderstanding", 
                  "author": "Camus", 
                  "year": 1944 
                },
                { "title": "The Just Assassins", 
                  "author": "Camus", 
                  "year": 1949 
                },
                { "title": "Pericles", 
                  "author": "Shakespeare", 
                  "year": 1609 
                }
            ]
}

//data5.json
{
    "data": [
                { "title": "Caligula", 
                  "author": "Camus", 
                  "year": 1944 
                },
                { "title": "Taming of the Shrew", 
                  "author": "Shakespeare", 
                  "year": 1623 
                },
                { "title": "Death of a Salesman", 
                  "author": "Miller", 
                  "year": 1949 
                }  
            ]
}
```

The following is the structure of the Array with objects:

```
[
  { title: 'Cymbeline', author: 'Shakespeare', year: 1623 },
  { title: 'The Tempest', author: 'Shakespeare', year: 1623 },
  { title: 'Hamlet', author: 'Shakespeare', year: 1603 },
  { title: "A Midsummer Night's Dream", author: 'Shakespeare', year: 1600},
  { title: 'Macbeth', author: 'Shakespeare', year: 1623 },
  { title: 'Glass Menagerie', author: 'Williams', year: 1944 },
  { title: 'The Misunderstanding', author: 'Camus', year: 1944 },
  { title: 'The Just Assassins', author: 'Camus', year: 1949 },
  { title: 'Pericles', author: 'Shakespeare', year: 1609 },
  { title: 'Caligula', author: 'Camus', year: 1944 },
  { title: 'Taming of the Shrew', author: 'Shakespeare', year: 1623 },      
  { title: 'Death of a Salesman', author: 'Miller', year: 1949 },
  { title: 'The Maid in the Mill', author: 'Fletcher', year: 1623 },        
  { title: 'A Streetcar Named Desire', author: 'Williams', year: 1947 },
  { title: 'King Lear', author: 'Shakespeare', year: 1608 }
]
```

- A NodeJS module with a function called `where()` that takes two inputs, an array 
  of objects and an object properties. It should return a new list containing only those objects that meet the key-value conditions in the object properties.

 The object property `{author: "Shakespeare"}` has the following new list structure:

```
[
  { title: 'Cymbeline', author: 'Shakespeare', year: 1623 },
  { title: 'The Tempest', author: 'Shakespeare', year: 1623 },
  { title: 'Hamlet', author: 'Shakespeare', year: 1603 },
  { title: "A Midsummer Night's Dream", author: 'Shakespeare', year: 1600 },
  { title: 'Macbeth', author: 'Shakespeare', year: 1623 },
  { title: 'Pericles', author: 'Shakespeare', year: 1609 },
  { title: 'Taming of the Shrew', author: 'Shakespeare', year: 1623 },
  { title: 'King Lear', author: 'Shakespeare', year: 1608 }
]
``` 

### Constrainsts

- It´s a must to use the `CommonJS Module` Pattern.
- It´s a must to use `Asynchronous Recursion with Callbacks` to traverse 
  directories.
- It´s important to use `Asynchronous operations` with NodeJS's event-driven APIs.
- It's important to use `callbacks`.
- Best practices to avoid `callback hell`.
- It's important to handle errors.
- Only use `filter` and `map` helper methods to reach the new list with objects.

### Sprints

1) Create project and install dependencies: `npm i`.
2) Read JSON files from `data` folder and create array with objects.
3) Test your module `readData.js`: `npm test`.
4) Get new list with objects that meet the key-value conditions.
5) Test your module `where.js`: `npm test`.
4) Test your project: `npm test`.

## Deliverables

- When you run 'npm start'...

You'll have in terminal…

```
$ npm start

New List for Shakespeare...

[
  { title: 'Cymbeline', author: 'Shakespeare', year: 1623 },
  { title: 'The Tempest', author: 'Shakespeare', year: 1623 },
  { title: 'Hamlet', author: 'Shakespeare', year: 1603 },
  { title: "A Midsummer Night's Dream", author: 'Shakespeare', year: 1600 },
  { title: 'Macbeth', author: 'Shakespeare', year: 1623 },
  { title: 'Pericles', author: 'Shakespeare', year: 1609 },
  { title: 'Taming of the Shrew', author: 'Shakespeare', year: 1623 },
  { title: 'King Lear', author: 'Shakespeare', year: 1608 }
]

```

- When you run 'npm test'...

You'll have in terminal…

```
$ npm test

> jest

 PASS  tests/where.test.js
  readData
    √ it should be data an instance of array (3 ms)
    √ it should read data length from array (3 ms)
    √ it should read number of key/properties from array (3 ms)
    √ it should read key and properties from array (7 ms)
  where function in test 1
    √ it should be data an instance of array (1 ms)
    √ it should read data length from array (1 ms)
    √ it should read number of key/properties from array (1 ms)
    √ it should read key and properties from array (1 ms)
  where function in test 2
    √ it should be data an instance of array (1 ms)
    √ it should read data length from array
    √ it should read number of key/properties from array
    √ it should read key and properties from array (1 ms)
  where function in test 3
    √ it should be data an instance of array (1 ms)
    √ it should read data length from array
  where function in test 4
    √ it should be data an instance of array
    √ it should read data length from array
    √ it should read number of key/properties from array
    √ it should read key and properties from array (1 ms)
  where function in test 5
    √ it should be data an instance of array
    √ it should read data length from array (1 ms)
    √ it should read number of key/properties from array (1 ms)
    √ it should read key and properties from array (1 ms)
  where function in test 6
    √ it should be data an instance of array
    √ it should read data length from array
    √ it should read number of key/properties from array

Test Suites: 1 passed, 1 total
Tests:       25 passed, 25 total
Snapshots:   0 total
Time:        0.565 s, estimated 2 s
Ran all test suites.
```
