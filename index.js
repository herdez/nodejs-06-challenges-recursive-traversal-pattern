//path module
const path = require('path')
//import readData()
//import where()


//values to test readJson()
const values = {author: "Shakespeare"}
//data directory
const dataDir = 'data'


//readData()
/*
*@param1: data directory path.
*@param2: callback to get array with objects and reach new list 
*         with selected objects.
*
*/

readData(path.join(__dirname, dataDir), () => {})