const { test, expect } = require('@jest/globals')
//const { describe } = require('yargs')

const { readData } = require('../readData')
const { where } = require('../where')

const dataList = [
    { title: 'Cymbeline', author: 'Shakespeare', year: 1623 },
    { title: 'The Tempest', author: 'Shakespeare', year: 1623 },
    { title: 'Hamlet', author: 'Shakespeare', year: 1603 },
    {
      title: "A Midsummer Night's Dream",
      author: 'Shakespeare',
      year: 1600
    },
    { title: 'Macbeth', author: 'Shakespeare', year: 1623 },
    { title: 'Glass Menagerie', author: 'Williams', year: 1944 },
    { title: 'The Misunderstanding', author: 'Camus', year: 1944 },
    { title: 'The Just Assassins', author: 'Camus', year: 1949 },
    { title: 'Pericles', author: 'Shakespeare', year: 1609 },
    { title: 'Caligula', author: 'Camus', year: 1944 },
    { title: 'Taming of the Shrew', author: 'Shakespeare', year: 1623 },      
    { title: 'Death of a Salesman', author: 'Miller', year: 1949 },
    { title: 'The Maid in the Mill', author: 'Fletcher', year: 1623 },        
    { title: 'A Streetcar Named Desire', author: 'Williams', year: 1947 },    
    { title: 'King Lear', author: 'Shakespeare', year: 1608 }
  ]

const plays = [
    { title: "Cymbeline", author: "Shakespeare", year: 1623 },
    { title: "The Tempest", author: "Shakespeare", year: 1623 },
    { title: "Hamlet", author: "Shakespeare", year: 1603 },
    { title: "The Maid in the Mill", author: "Fletcher", year: 1623 },
    { title: "A Streetcar Named Desire", author: "Williams", year: 1947 },
    { title: "King Lear", author: "Shakespeare", year: 1608 },
    { title: "A Midsummer Night's Dream", author: "Shakespeare", year: 1600 },
    { title: "Macbeth", author: "Shakespeare", year: 1623 },
    { title: "Glass Menagerie", author: "Williams", year: 1944 },
    { title: "The Misunderstanding", author: "Camus", year: 1944 },
    { title: "The Just Assassins", author: "Camus", year: 1949 },
    { title: "Pericles", author: "Shakespeare", year: 1609 },
    { title: "Caligula", author: "Camus", year: 1944 },
    { title: "Taming of the Shrew", author: "Shakespeare", year: 1623 },
    { title: "Death of a Salesman", author: "Miller", year: 1949 },
  ]


//test readData()
describe('readData', () => {
    test('it should be data an instance of array', done => {
        readData('./data', (err, data) => {
            expect( data instanceof Array ).toBeTruthy()
            done()
        })
    })

    test('it should read data length from array', done => {
        readData('./data', (err, data) => {
            expect( data.length ).toBe(15)
            done()
        })
    })

    test('it should read number of key/properties from array', done => {
        readData('./data', (err, data) => {
            const listOfObjects = [...new Set(data.map(value => Object.entries(value)).flat(Infinity))]
            expect(listOfObjects.length).toBe(31)
            done()
        })
    })

    test('it should read key and properties from array', done => {
        readData('./data', (err, data) => {
            const listOfObjects = [...new Set(data.map(value => Object.entries(value)).flat(Infinity))]
            const output = 0
            
            expect(listOfObjects.indexOf('title')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Cymbeline')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('author')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Shakespeare')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('year')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(1623)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('The Tempest')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Hamlet')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(1603)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf("A Midsummer Night's Dream")).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(1600)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Macbeth')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Glass Menagerie')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Williams')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(1944)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('The Misunderstanding')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Camus')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('The Just Assassins')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(1949)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Pericles')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(1609)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Caligula')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Taming of the Shrew')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Death of a Salesman')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('The Maid in the Mill')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('Fletcher')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('A Streetcar Named Desire')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(1947)).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf('King Lear')).toBeGreaterThanOrEqual(output)
            expect(listOfObjects.indexOf(1608)).toBeGreaterThanOrEqual(output)
            done()
        })
    })

})


//test 1 - where()
describe('where function in test 1', () => {
    test('it should be data an instance of array', () => {
        const objectInput = {author: "Shakespeare"}
        const functionInput = where(plays, objectInput)
        expect( functionInput instanceof Array ).toBeTruthy()
    })

    test('it should read data length from array', () => {
        const objectInput = {author: "Shakespeare"}
        const functionInput = where(plays, objectInput)
        expect( functionInput.length).toBe(8)      
    })

    test('it should read number of key/properties from array', () => {
        const objectInput = {author: "Shakespeare"}
        const functionInput = where(plays, objectInput)        
        const listOfObjects = [...new Set(functionInput.map(value => Object.entries(value)).flat(Infinity))]
        expect(listOfObjects.length).toBe(17)
    })

    test('it should read key and properties from array', () => {
        const objectInput = {author: "Shakespeare"}
        const functionInput = where(plays, objectInput)        
        const listOfObjects = [...new Set(functionInput.map(value => Object.entries(value)).flat(Infinity))]
        const output = 0
            
        expect(listOfObjects.indexOf(1623)).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf(1603)).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf(1608)).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf(1600)).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf(1609)).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf("A Midsummer Night's Dream")).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf(1609)).toBeGreaterThanOrEqual(output)
    })

})


//test 2 - where()
describe('where function in test 2', () => {
    test('it should be data an instance of array', () => {
        const objectInput = {year: 1944}
        const functionInput = where(plays, objectInput)
        expect( functionInput instanceof Array ).toBeTruthy()
    })

    test('it should read data length from array', () => {
        const objectInput = {year: 1944}
        const functionInput = where(plays, objectInput)
        expect( functionInput.length).toBe(3)      
    })

    test('it should read number of key/properties from array', () => {
        const objectInput = {year: 1944}
        const functionInput = where(plays, objectInput)        
        const listOfObjects = [...new Set(functionInput.map(value => Object.entries(value)).flat(Infinity))]
        expect(listOfObjects.length).toBe(9)
    })

    test('it should read key and properties from array', () => {
        const objectInput = {year: 1944}
        const functionInput = where(plays, objectInput)        
        const listOfObjects = [...new Set(functionInput.map(value => Object.entries(value)).flat(Infinity))]
        const output = 0
            
        expect(listOfObjects.indexOf('title')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('Glass Menagerie')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('author')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('Williams')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('year')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf(1944)).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('The Misunderstanding')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('Camus')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('Caligula')).toBeGreaterThanOrEqual(output)
    })

})

//test 3 - where()
describe('where function in test 3', () => {
    test('it should be data an instance of array', () => {
        const objectInput = {year: 1611}
        const functionInput = where(plays, objectInput)
        expect( functionInput instanceof Array ).toBeTruthy()
    })

    test('it should read data length from array', () => {
        const objectInput = {year: 1611}
        const functionInput = where(plays, objectInput)
        expect( functionInput.length).toBe(0)      
    })
})

//test 4 - where()
describe('where function in test 4', () => {
    test('it should be data an instance of array', () => {
        const objectInput = {author: "Shakespeare", year: 1623}
        const functionInput = where(plays, objectInput)
        expect( functionInput instanceof Array ).toBeTruthy()
    })

    test('it should read data length from array', () => {
        const objectInput = {author: "Shakespeare", year: 1623}
        const functionInput = where(plays, objectInput)
        expect( functionInput.length).toBe(4)      
    })

    test('it should read number of key/properties from array', () => {
        const objectInput = {author: "Shakespeare", year: 1623}
        const functionInput = where(plays, objectInput)        
        const listOfObjects = [...new Set(functionInput.map(value => Object.entries(value)).flat(Infinity))]
        expect(listOfObjects.length).toBe(9)
    })

    test('it should read key and properties from array', () => {
        const objectInput = {author: "Shakespeare", year: 1623}
        const functionInput = where(plays, objectInput)        
        const listOfObjects = [...new Set(functionInput.map(value => Object.entries(value)).flat(Infinity))]
        const output = 0
            
        expect(listOfObjects.indexOf('Cymbeline')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('title')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('author')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('year')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf(1623)).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('The Tempest')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('Macbeth')).toBeGreaterThanOrEqual(output)
    })
})

//test 5 - where()
describe('where function in test 5', () => {
    test('it should be data an instance of array', () => {
        const objectInput = {author: "Camus", year: 1944}
        const functionInput = where(plays, objectInput)
        expect( functionInput instanceof Array ).toBeTruthy()
    })

    test('it should read data length from array', () => {
        const objectInput = {author: "Camus", year: 1944}
        const functionInput = where(plays, objectInput)
        expect( functionInput.length).toBe(2)      
    })

    test('it should read number of key/properties from array', () => {
        const objectInput = {author: "Camus", year: 1944}
        const functionInput = where(plays, objectInput)        
        const listOfObjects = [...new Set(functionInput.map(value => Object.entries(value)).flat(Infinity))]
        expect(listOfObjects.length).toBe(7)
    })

    test('it should read key and properties from array', () => {
        const objectInput = {author: "Camus", year: 1944}
        const functionInput = where(plays, objectInput)        
        const listOfObjects = [...new Set(functionInput.map(value => Object.entries(value)).flat(Infinity))]
        const output = 0
            
        expect(listOfObjects.indexOf('Camus')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('title')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('author')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('year')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf(1944)).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('The Misunderstanding')).toBeGreaterThanOrEqual(output)
        expect(listOfObjects.indexOf('Caligula')).toBeGreaterThanOrEqual(output)
    })
})

//test 6 - where()
describe('where function in test 6', () => {
    test('it should be data an instance of array', () => {
        const objectInput = { title: "Pericles", author: "Shakespeare", year: 1609 }
        const functionInput = where(plays, objectInput)
        expect( functionInput instanceof Array ).toBeTruthy()
    })

    test('it should read data length from array', () => {
        const objectInput = { title: "Pericles", author: "Shakespeare", year: 1609 }
        const functionInput = where(plays, objectInput)
        expect( functionInput.length).toBe(1)      
    })

    test('it should read number of key/properties from array', () => {
        const objectInput = { title: "Pericles", author: "Shakespeare", year: 1609 }
        const functionInput = where(plays, objectInput)        
        const listOfObjects = [...new Set(functionInput.map(value => Object.entries(value)).flat(Infinity))]
        expect(listOfObjects.length).toBe(6)
    })
})